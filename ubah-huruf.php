<?php
function ubah_huruf($string)
{
    //kode di sini
    $n = '';
    for ($i = 0; $i < strlen($string); $i++) {
        $n .= chr(ord($string[$i]) + 1);
        // $n .= ++$string[$i];
    }

    return $n . '<br>';
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
